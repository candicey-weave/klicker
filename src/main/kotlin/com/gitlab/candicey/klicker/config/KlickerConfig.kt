package com.gitlab.candicey.klicker.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.Slider
import cc.polyfrost.oneconfig.config.annotations.Switch
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize

private const val LEFT_CATEGORY = "Left"
private const val RIGHT_CATEGORY = "Right"

object KlickerConfig : Config(Mod("Klicker", ModType.PVP, "/klicker/logo.svg", null), "klicker.json") {
    @Switch(
        name = "Enabled",
        subcategory = LEFT_CATEGORY,
        size = OptionSize.DUAL,
    )
    var leftEnabled = true
    
    @Slider(
        name = "Double Click Chance",
        description = "Chance that the left click will be double",
        min = 0f,
        max = 100f,
        step = 1,
        subcategory = LEFT_CATEGORY,
    )
    var leftChance = 50

    @Slider(
        name = "Next Tick Chance",
        description = "Chance that the double click will be on the next tick",
        min = 0f,
        max = 100f,
        step = 1,
        subcategory = LEFT_CATEGORY,
    )
    var leftNextTickChance = 30

    @Switch(
        name = "Enabled",
        subcategory = RIGHT_CATEGORY,
        size = OptionSize.DUAL,
    )
    var rightEnabled = false
    
    @Slider(
        name = "Double Click Chance",
        description = "Chance that the right click will be double",
        min = 0f,
        max = 100f,
        step = 1,
        subcategory = RIGHT_CATEGORY,
    )
    var rightChance = 50

    @Slider(
        name = "Next Tick Chance",
        description = "Chance that the double click will be on the next tick",
        min = 0f,
        max = 100f,
        step = 1,
        subcategory = RIGHT_CATEGORY,
    )
    var rightNextTickChance = 30

    init {
        initialize()
    }
}
