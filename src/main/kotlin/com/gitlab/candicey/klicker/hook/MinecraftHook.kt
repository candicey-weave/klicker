package com.gitlab.candicey.klicker.hook

import com.gitlab.candicey.klicker.config.KlickerConfig
import com.gitlab.candicey.klicker.util.ProbabilityUtil
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import net.minecraft.client.settings.KeyBinding
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes.INVOKESTATIC
import org.objectweb.asm.Opcodes.INVOKEVIRTUAL
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.JumpInsnNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.MethodInsnNode

object MinecraftHook : Hook("net/minecraft/client/Minecraft") {
    private var leftClicks = mutableListOf<Click>()
    private var rightClicks = mutableListOf<Click>()

    private val attackKeyCode: Int
        get() = mc.gameSettings.keyBindAttack.keyCode
    private val useItemKeyCode: Int
        get() = mc.gameSettings.keyBindUseItem.keyCode

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        with(node.methods.named("runTick")) {
            val mouseNext = instructions.find { it.opcode == INVOKESTATIC && with(it as MethodInsnNode) { owner == "org/lwjgl/input/Mouse" && name == "next" && desc == "()Z" } } ?: error("Could not find mouseNext")

            val leftClickCountLocal = maxLocals + 1
            val rightClickCountLocal = leftClickCountLocal + 1

            val invokeVirtualBeforeMouseNext = run {
                for (i in instructions.indexOf(mouseNext) downTo 0) {
                    val insnNode = instructions[i]
                    if (insnNode.opcode == INVOKEVIRTUAL) {
                        return@run insnNode
                    }
                }
                error("Could not find label before mouseNext")
            }

            val jumpNoMouseClick = mouseNext.next as JumpInsnNode

            instructions.insert(invokeVirtualBeforeMouseNext, asm {
                iconst_0
                istore(leftClickCountLocal)
                iconst_0
                istore(rightClickCountLocal)
            })

            instructions.insert(jumpNoMouseClick, asm {
                val labelLeft = LabelNode()
                val labelRight = LabelNode()

                aload(0)
                getfield("net/minecraft/client/Minecraft", "gameSettings", "Lnet/minecraft/client/settings/GameSettings;")
                dup
                getfield("net/minecraft/client/settings/GameSettings", "keyBindAttack", "Lnet/minecraft/client/settings/KeyBinding;")
                invokevirtual("net/minecraft/client/settings/KeyBinding", "getKeyCode", "()I")
                invokestatic("org/lwjgl/input/Mouse", "getEventButton", "()I")
                bipush(100)
                isub
                if_icmpne(labelLeft)
                invokestatic("org/lwjgl/input/Mouse", "getEventButtonState", "()Z")
                ifeq(labelLeft)
                iload(leftClickCountLocal)
                iconst_1
                iadd
                istore(leftClickCountLocal)
                +labelLeft
                getfield("net/minecraft/client/settings/GameSettings", "keyBindUseItem", "Lnet/minecraft/client/settings/KeyBinding;")
                invokevirtual("net/minecraft/client/settings/KeyBinding", "getKeyCode", "()I")
                invokestatic("org/lwjgl/input/Mouse", "getEventButton", "()I")
                bipush(100)
                isub
                if_icmpne(labelRight)
                invokestatic("org/lwjgl/input/Mouse", "getEventButtonState", "()Z")
                ifeq(labelRight)
                iload(rightClickCountLocal)
                iconst_1
                iadd
                istore(rightClickCountLocal)
                +labelRight
            })

            instructions.insert(jumpNoMouseClick.label, asm {
                iload(leftClickCountLocal)
                iload(rightClickCountLocal)
                invokestatic(internalNameOf<MinecraftHook>(), ::processClicks.name, "(II)V")
                invokestatic(internalNameOf<MinecraftHook>(), ::click.name, "()V")
            })
        }

        cfg.computeFrames()
    }

    @JvmStatic
    fun click() {
        fun click(clicks: MutableList<Click>, keyCode: Int, clickListener: () -> Unit) {
            clicks.removeIf {
                if (it.tickLeft == 0) {
                    KeyBinding.setKeyBindState(keyCode, true)
                    KeyBinding.onTick(keyCode)
                    clickListener()
                    KeyBinding.setKeyBindState(keyCode, false)
                    KeyBinding.onTick(keyCode)

                    true
                } else {
                    it.tickLeft--

                    false
                }
            }
        }

        click(leftClicks, attackKeyCode) { LunarCpsHandlerHook.lunarCpsHandlerInstance!!.addLeftClick(System.currentTimeMillis()) }
        click(rightClicks, useItemKeyCode) { LunarCpsHandlerHook.lunarCpsHandlerInstance!!.addRightClick(System.currentTimeMillis()) }
    }

    @JvmStatic
    fun processClicks(left: Int, right: Int) {
        if (!KlickerConfig.enabled) {
            return
        }

        if (left != 0 && KlickerConfig.leftEnabled) {
            leftClicks.addAll(generateClicks(left, KlickerConfig.leftChance, KlickerConfig.leftNextTickChance))
        }

        if (right != 0 && KlickerConfig.rightEnabled) {
            rightClicks.addAll(generateClicks(right, KlickerConfig.rightChance, KlickerConfig.rightNextTickChance))
        }
    }

    private fun generateClicks(amount: Int, chance: Int, nextTickChance: Int): List<Click> {
        val clicks = mutableListOf<Click>()

        repeat(amount) {
            if (ProbabilityUtil.chance(chance)) {
                clicks += Click(if (ProbabilityUtil.chance(nextTickChance)) 1 else 0)
            }
        }

        return clicks
    }

    private class Click(var tickLeft: Int)
}
