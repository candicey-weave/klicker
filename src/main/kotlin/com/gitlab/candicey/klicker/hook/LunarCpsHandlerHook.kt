package com.gitlab.candicey.klicker.hook

import com.gitlab.candicey.zenithcore.extension.insertBeforeReturn
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes.ACC_PUBLIC
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.InsnList
import org.objectweb.asm.tree.MethodNode

class LunarCpsHandlerHook(lunarCpsHandlerClassName: String) : Hook(lunarCpsHandlerClassName) {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        with(node.methods) {
            with(named("<init>")) {
                instructions.insertBeforeReturn(asm {
                    aload(0)
                    invokestatic(internalNameOf<LunarCpsHandlerHook>(), "setLunarCpsHandlerInstance", "(L${internalNameOf<LunarCpsHandlerAccessor>()};)V")
                })
            }

            val (leftClickLongListFieldName, rightClickLongListFieldName) = node.fields.filter { it.desc == "Lit/unimi/dsi/fastutil/longs/LongList;" }.map(FieldNode::name)

            fun MutableList<MethodNode>.add(methodName: String, methodDesc: String, insnList: InsnList) = add(MethodNode(ACC_PUBLIC, methodName, methodDesc, null, null).apply { instructions = insnList })

            add(LunarCpsHandlerAccessor::getLeftClickLongList.name, "()Ljava/lang/Object;", asm {
                aload(0)
                getfield(node.name, leftClickLongListFieldName, "Lit/unimi/dsi/fastutil/longs/LongList;")
                areturn
            })

            add(LunarCpsHandlerAccessor::getRightClickLongList.name, "()Ljava/lang/Object;", asm {
                aload(0)
                getfield(node.name, rightClickLongListFieldName, "Lit/unimi/dsi/fastutil/longs/LongList;")
                areturn
            })

            add(LunarCpsHandlerAccessor::addLeftClick.name, "(J)V", asm {
                aload(0)
                getfield(node.name, leftClickLongListFieldName, "Lit/unimi/dsi/fastutil/longs/LongList;")
                lload(1)
                invokeinterface("it/unimi/dsi/fastutil/longs/LongList", "add", "(J)Z")
                pop
                _return
            })

            add(LunarCpsHandlerAccessor::addRightClick.name, "(J)V", asm {
                aload(0)
                getfield(node.name, rightClickLongListFieldName, "Lit/unimi/dsi/fastutil/longs/LongList;")
                lload(1)
                invokeinterface("it/unimi/dsi/fastutil/longs/LongList", "add", "(J)Z")
                pop
                _return
            })
        }

        node.interfaces.add(internalNameOf<LunarCpsHandlerAccessor>())

        cfg.computeFrames()
    }

    companion object {
        @JvmStatic
        var lunarCpsHandlerInstance: LunarCpsHandlerAccessor? = null
    }

    interface LunarCpsHandlerAccessor {
        fun getLeftClickLongList(): Any

        fun getRightClickLongList(): Any

        fun addLeftClick(time: Long)

        fun addRightClick(time: Long)
    }
}