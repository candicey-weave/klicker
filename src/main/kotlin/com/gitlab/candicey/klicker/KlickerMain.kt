package com.gitlab.candicey.klicker

import com.gitlab.candicey.klicker.hook.LunarCpsHandlerHook
import com.gitlab.candicey.klicker.hook.MinecraftHook
import com.gitlab.candicey.zenithcore.extension.add
import com.gitlab.candicey.zenithcore.helper.HookManagerHelper
import com.gitlab.candicey.zenithcore.lunarClasses
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.Dependencies.concentra
import com.gitlab.candicey.zenithloader.dependency.version
import net.weavemc.loader.api.ModInitializer

class KlickerMain : ModInitializer {
    override fun preInit() {
        info("Initialising...")

        ZenithLoader.loadDependencies(
            concentra version "klicker",
        )

        val lunarCpsHandlerClass = lunarClasses.find { it.fields.filter { field -> field.desc == "Lit/unimi/dsi/fastutil/longs/LongList;" }.size == 2 } ?: error("Could not find lunarCpsHandlerClassName")

        HookManagerHelper.hooks.add(
            MinecraftHook,
            LunarCpsHandlerHook(lunarCpsHandlerClass.name),
        )

        info("Initialised!")
    }
}