package com.gitlab.candicey.klicker.util

object ProbabilityUtil {
    private val percentRange = 0..100

    fun chance(chance: Int): Boolean = percentRange.random() <= chance
}