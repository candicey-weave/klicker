package com.gitlab.candicey.klicker

fun info(message: String) = println("[Klicker] $message")