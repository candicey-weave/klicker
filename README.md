# Klicker
- A simple double-clicker for [Weave](https://github.com/Weave-MC) mod-loader written in Kotlin.

<br>

## Features
![Config](assets/config.png)

<br>

## Installation
1. Download the [Klicker](#download) mod.
2. Place the jars in your Weave mods folder.
    1. Windows: `%userprofile%\.weave\mods`
    2. Unix: `~/.weave/mods`

<br>

## Build
- Clone the repository.
- Run `./gradlew build` in the root directory.
- The built jar file will be in `build/libs`.

<br>

## Download
- [Package Registry](https://gitlab.com/candicey-weave/klicker/-/packages) - Select the latest version and download the jar file that ended with `-relocated.jar`.

<br>

## License
- Klicker is licensed under the [GNU General Public License Version 3](LICENSE).
